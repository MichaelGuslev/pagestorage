﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PageStorageDAL.Models;
using PageStorageDAL.EfInfrastructure;

namespace PageStorageDAL.Repositories
{
    public class BaseRepository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : BaseEntity, new()
    {
        private DbContext _context;
        private DbSet<TEntity> _dbset;

        public BaseRepository(PageStorageContext context)
        {
            _context = context;
            _dbset = _context.Set<TEntity>();
        }

        //TODO: create destructor with Dispose() call

        public void Dispose()
        {
            _context?.Dispose();
        }

        public int Add(TEntity entity)
        {
            _dbset.Add(entity);
            return _context.SaveChanges();
        }

        public int Delete(TEntity entity)
        {
            _dbset.Remove(entity);
            return _context.SaveChanges();
        }

        public int Delete(int id)
        {
            _context.Entry(new TEntity() { Id = id })
                .State = EntityState.Deleted;
            return _context.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbset.ToList();
        }

        public TEntity GetOne(int id)
        {
            return _dbset.Find(id);
        }

        public IEnumerable<TEntity> GetSome(Expression<Func<TEntity, bool>> where)
        {
            return _dbset.Where(where).ToList();
        }

        public int Update(TEntity entity)
        {
            _dbset.Update(entity);
            return _context.SaveChanges();
        }
    }
}
