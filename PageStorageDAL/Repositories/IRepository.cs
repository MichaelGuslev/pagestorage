﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;
using PageStorageDAL.Models;

namespace PageStorageDAL.Repositories
{
    //TODO: make async repo
    public interface IRepository<TEntity> where TEntity : BaseEntity, new()
    {
        TEntity GetOne(int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetSome(Expression<Func<TEntity, bool>> where);

        int Add(TEntity entity);
        int Update(TEntity entity);
        int Delete(TEntity entity);
        int Delete(int id);
    }
}
