﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace PageStorageDAL.EfInfrastructure
{
    internal class PageStorageContextFactory : IDesignTimeDbContextFactory<PageStorageContext>
    {
        public PageStorageContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddUserSecrets<PageStorageContext>()
                .Build();

            var storageConnectionString = configuration.GetConnectionString("PageStorage");

            var options = new DbContextOptionsBuilder<PageStorageContext>()
                .UseNpgsql(storageConnectionString)
                .Options;

            return new PageStorageContext(options);
        }
    }
}
