﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PageStorageDAL.Models;

namespace PageStorageDAL.EfInfrastructure
{
    //TODO: make it internal, create service wrapper
    public class PageStorageContext : DbContext
    {
        public DbSet<Page> Pages { get; private set; }

        public PageStorageContext(DbContextOptions options) : base(options)
        {

        }

        //TODO: override SaveChangesAsync
        public override int SaveChanges()
        {
            OnBefoSaving();
            return base.SaveChanges();
        }

        private void OnBefoSaving()
        {
            //TODO: replace this crutch
            var timestamp = DateTime.UtcNow;

            ChangeTracker.Entries()
                .Where(e => e.State 
                    is EntityState.Modified 
                    or EntityState.Added)
                .Select(e => e.Entity)
                .Cast<BaseEntity>()
                .ToList()
                .ForEach(e => e.Timestamp = timestamp);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Page>(page =>
            {
                page.HasIndex(page => page.Title)
                    .IsUnique();
            });
        }
    }
}
