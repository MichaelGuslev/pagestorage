﻿using System.ComponentModel.DataAnnotations;

namespace PageStorageDAL.Models
{
    public class Page : BaseEntity
    {
        //TODO: create default ctor with 'Obsolete("Only for reflection", true)' attribute
        //TODO: create public ctor with arguments
        [Required]
        public string Title { get; set; }
        [Required]
        public string Snippet { get; set; }
    }
}
