﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PageStorageAPI.Dto;
using PageStorageDAL.Models;
using PageStorageDAL.Repositories;

namespace PageStorageAPI.DomainServices
{
    public class PageService
    {
        private IRepository<Page> _repo;
        public PageService(IRepository<Page> repository)
        {
            _repo = repository;
        }

        public void AddNewPage(PageDto dto)
        {
            CheckUniquenessOfTitle(dto.Title);

            Page newPage = new()
            {
                Title = dto.Title,
                Snippet = dto.Snippet
            };

            _repo.Add(newPage);
        }

        public void UpdatePage(int id, PageDto dto)
        {
            CheckUniquenessOfTitle(dto.Title);

            Page updatedPage = new()
            {
                Id = id,
                Title = dto.Title,
                Snippet = dto.Snippet
            };

            _repo.Update(updatedPage);
        }

        public void DeletePage(int id)
        {
            _repo.Delete(id);
        }

        private void CheckUniquenessOfTitle(string title)
        {
            //TODO: need to be implemented
        }
    }
}
