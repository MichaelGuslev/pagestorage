﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PageStorageAPI.Dto
{
    public class PageDto
    {
        public string Title { get; init; }
        public string Snippet { get; init; }
    }
}
