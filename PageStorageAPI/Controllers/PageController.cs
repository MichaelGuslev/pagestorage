﻿using Microsoft.AspNetCore.Mvc;
using PageStorageAPI.Dto;
using PageStorageDAL.Models;
using PageStorageDAL.Repositories;
using PageStorageAPI.DomainServices;

namespace PageStorageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PageController : ControllerBase
    {
        private PageService _pageService;

        public PageController(PageService service)
        {
            _pageService = service;
        }

        // POST api/<PageController>
        [HttpPost]
        public void Post([FromBody] PageDto page)
        {
            _pageService.AddNewPage(page);
        }

        // PUT api/<PageController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] PageDto page)
        {
            _pageService.UpdatePage(id, page);
        }

        // DELETE api/<PageController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            //TODO: add checking timestamp
            _pageService.DeletePage(id);
        }
    }
}
