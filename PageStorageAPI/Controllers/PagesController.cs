﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PageStorageDAL.Models;
using PageStorageDAL.Repositories;

namespace PageStorageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagesController : ControllerBase
    {
        private IRepository<Page> _repository;

        public PagesController(IRepository<Page> repository)
        {
            _repository = repository;
        }

        // GET: api/<PagesController>
        [HttpGet]
        public IEnumerable<Page> Get()
        {
            return _repository.GetAll();
        }

        //TODO: create an action to get pages with some text
    }
}
